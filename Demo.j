.class public Demo
.super java/lang/Object

;
; standard initializer
.method public <init>()V
   aload_0
   invokenonvirtual java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V
       ; set limits used by this method
       .limit locals 10
       .limit stack 256

       ; setup local variables:

       ;    1 - the PrintStream object held in java.lang.out
       getstatic java/lang/System/out Ljava/io/PrintStream;

       ; place your bytecodes here
       ; START

       new frame_0
       dup
       invokespecial frame_0/<init>()V
       astore 0
       aload 0
       new closure_00
       dup
       invokespecial closure_00/<init>()V
       putfield frame_0/loc_00 Ltype_00;
       
       ; Call
       
       ; Retriving id= fact
       aload 0
       checkcast frame_0
       getfield frame_0/loc_00 Ltype_00;
       sipush 5
       invokeinterface type_00/call(I)I 2
       
       ; Pop Frame
       aconst_null
       astore 0
       ; END


       ; convert to String;
       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
       ; call println 
       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

       return

.end method
