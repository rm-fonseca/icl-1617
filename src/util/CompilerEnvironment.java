package util;
import java.util.*;

public class CompilerEnvironment {

	static class Assoc<T> {
		String id;
		T value;
		
		Assoc(String id, T value) {
			this.id = id;
			this.value = value;
		}
		
	}

	CompilerEnvironment up;
	ArrayList<Assoc<Integer>> assocs;
	
	public CompilerEnvironment() {
		this.up = null;
		this.assocs = new ArrayList<Assoc<Integer>>();
	}
	
	private CompilerEnvironment(CompilerEnvironment up) {
		this();
		this.up = up;
	}
	
	public class Address {
		int jumps;
		int offset;
		
		public Address(int jumps, int offset){
			this.jumps = jumps;
			this.offset = offset;
		}
		
		public int getJumps(){
			return jumps;
		}
		
		public int getOffset(){
			return offset;
		}
	}
	
	public Address lookup(String id) throws UndeclaredIdentifierException {
		int jumps = 0;
		
		CompilerEnvironment current = this;
		while(current != null) {
			for(Assoc<Integer> assoc : current.assocs)
				if(assoc.id.equals(id))
					return new Address(jumps, assoc.value);
			current = current.up;
			jumps++;
		}
		throw new UndeclaredIdentifierException(id);
	}
	
	public CompilerEnvironment beginScope() {
		return new CompilerEnvironment(this);
	}
	
	public CompilerEnvironment endScope() {
		return up;
	}
	
	public void assoc( String id, Integer value ) throws DuplicateIdentifierException {

		for(Assoc<Integer> assoc: assocs)
			if(assoc.id.equals(id))
				throw new DuplicateIdentifierException(id);
		
		assocs.add(new Assoc<Integer>(id,value));
		
	}

	public int addId(String id) throws DuplicateIdentifierException {
		int offset = assocs.size();
		assoc(id, offset);
		return offset;
	}

}


