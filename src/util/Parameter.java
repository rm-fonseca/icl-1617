package util;

import Type.IType;


public class Parameter {

	private String id;
	private IType type;

	public Parameter(String id, IType type) {
		super();
		this.id = id;

		this.type = type;
	}

	public String getId() {
		return id;
	}

	public IType getType() {
		return type;
	}

}
