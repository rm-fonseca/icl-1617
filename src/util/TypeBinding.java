package util;
import AST.ASTNode;
import Type.IType;

public class TypeBinding {
	
	private String id;
	private ASTNode expr;
	private IType type;

	public TypeBinding(String id, ASTNode expr,IType type) {
		this.id = id;
		this.expr = expr;
		this.type = type;
	}

	public String getId() {
		return this.id;
	}

	public ASTNode getExpr() {
		return expr;
	}
	public IType getType() {
		return type;
	}
	
	
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
				
		return expr.typecheck(env);

	}
}

