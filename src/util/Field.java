package util;


import Type.IType;
import values.IValue;

public class Field {

	private String id;
	private IValue value;

	public Field(String id,IValue value) {
		this.id = id;
		this.value = value;
	}

	public String getId() {
		return this.id;
	}

	public IValue getExpr() {
		return value;
	}
	
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
				
		return value.getType();
	}
	
}
