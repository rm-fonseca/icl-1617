package util;
import AST.ASTNode;
import Type.IType;

public class Binding {
	
	private String id;
	private ASTNode expr;

	public Binding(String id, ASTNode expr) {
		this.id = id;
		this.expr = expr;
	}

	public String getId() {
		return this.id;
	}

	public ASTNode getExpr() {
		return expr;
	}
	
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
				
		return expr.typecheck(env);

	}
}

