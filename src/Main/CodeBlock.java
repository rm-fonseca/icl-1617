package Main;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import javax.script.Bindings;

import AST.ASTNode;
import AST.value.Closure;
import Main.CodeBlock.StackFrame;
import Type.BoolType;
import Type.ClosureType;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.Binding;
import util.CompilerEnvironment;
import util.CompilerEnvironment.Address;
import util.DuplicateIdentifierException;
import util.Parameter;
import util.UndeclaredIdentifierException;

public class CodeBlock {

	public static final String LABEL = "Label_";

	static public class Ref {

		private RefType ref;

		Ref(IType ref) {
			this.ref = (RefType) ref;

		}

		public void createRefFile() {

			// open file
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(ref.getName() + ".j", "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			// write header

			writer.println(".source " + ref.getName() + ".j");
			writer.println(".class " + ref.getName());
			writer.println(".super java/lang/Object");

			// write ndecls field declarations
			writer.println("");

			writer.print(".field public value ");

			
			
			if (ref.getType() == IntType.singleton ||ref.getType() == BoolType.singleton)
				writer.println("I");
			else
				writer.println("L" + ref.getType().compileType() + ";");
		

			writer.println("");

			// write footer
			writer.println(".method public <init>()V");
			writer.println("aload_0");
			writer.println("invokespecial java/lang/Object/<init>()V");
			writer.println("return");
			writer.println(".end method");

			// Close file
			writer.close();

		}

		public String getType() {
			return ref.getName();
		}
	}

	static public class ClosureFrame {

		private CompilerEnvironment env;
		private ASTNode body;
		private List<Parameter> params;
		private ClosureType type;

		public ClosureFrame(CompilerEnvironment env, ASTNode body, List<Parameter> params, ClosureType type) {
			this.env = env;
			this.body = body;
			this.params = params;
			this.type = type;
		}

		public void dump() throws DuplicateIdentifierException, UndeclaredIdentifierException {

			// Interface
			PrintStream writer = null;
			try {
				writer = new PrintStream(new FileOutputStream(type.compileType() + ".j"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			String callParams = "";

			for (Parameter p : params) {

				if (p.getType() == IntType.singleton || p.getType() == BoolType.singleton)
					callParams += "I";
				else
					callParams += "L"+p.getType().compileType()+";";
				env.addId(p.getId());
			}


/*			String returnType = "";

			if (body.getType() instanceof IntType)
				returnType = "I";
*/
			
			String returnType = "";
			if (body.getType() == IntType.singleton || body.getType() == BoolType.singleton)
				returnType += "I";
			else
				returnType += "L"+body.getType().compileType()+";";
			
			writer.println(".source " + type.compileType() + ".j");
			writer.println(".interface public " + type.compileType());
			writer.println(".super java/lang/Object");
			writer.println(".method public abstract call(" + callParams + ")" + returnType);
			writer.println(".end method");

			// Close file
			writer.close();

			// Closure

			// open file
			try {
				writer = new PrintStream(new FileOutputStream(type.getClassName() + ".j"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			// write header

			writer.println(".source " + type.getClassName() + ".j");
			writer.println(".class public " + type.getClassName());
			writer.println(".super java/lang/Object");
			writer.println(".implements " + type.compileType());
			// write ndecls field declarations
			writer.println("");

			// write footer
			writer.println(".method public <init>()V");
			writer.println("aload_0");
			writer.println("invokespecial java/lang/Object/<init>()V");
			writer.println("return");
			writer.println(".end method");
			writer.println("");

			writer.println(".method public call(" + callParams + ")" + returnType);
			writer.println("");
			writer.println(".limit locals 3 ; 0 - this, 1 - parameter, 2 - SP ");
			writer.println(".limit stack 256");

			CodeBlock closureCode = new CodeBlock("2");
			StackFrame frame = closureCode.createFrame(null, params);

			closureCode.emit_new(frame.getType());
			closureCode.emit_dup();
			closureCode.emit_invokespecial(frame.getType() + "/<init>()V");
			for (int i = 0; i < params.size(); i++) {
				closureCode.emit_dup();
				String type = "";

				if (params.get(i).getType() == IntType.singleton || params.get(i).getType() == BoolType.singleton){
					type = "I";
					closureCode.emit_iload(i+1);
				}
				else if (params.get(i).getType() instanceof RefType){
					type += "L"+((RefType) params.get(i).getType()).getName()+";";
					closureCode.emit_aload(i+1);
				}

				closureCode.emit_putfield(frame.getType() + "/loc_" + String.format("%02d", i) + " " + type);
			}

			/*
			 * ; initialize new stackframe frame_1 new frame_1 dup invokespecial
			 * frame_1/<init>()V dup iload 1 putfield frame_1/ 00 I astore 2
			 */
			closureCode.pushFrame(frame);
			body.compile(closureCode, env);

			closureCode.dumpCode(writer);

			if (body.getType() == IntType.singleton || body.getType() == BoolType.singleton)
				writer.println("	ireturn");
			else
				writer.println("	areturn");
			
			writer.println(".end method");

			// Close file
			writer.close();

			closureCode.dumpFrames();

		}

	}

	static public class StackFrame {
		int number;
		List<Binding> ndecls;
		List<Parameter> params;
		StackFrame parentFrame;
		static int fnumber = 0;
		public static final String NAME = "stackFrame";

		StackFrame(List<Binding> decl, StackFrame parentFrame, List<Parameter> params) {
			this.number = fnumber++;
			this.ndecls = decl;
			this.params = params;
			this.parentFrame = parentFrame;
		}

		void dump() {

			// open file
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(getType() + ".j", "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			// write header

			writer.println(".source " + getType() + ".j");
			writer.println(".class " + getType());
			writer.println(".super java/lang/Object");

			// write ndecls field declarations
			writer.println("");

			if (parentFrame != null) {
				writer.println(".field public SL L" + parentFrame.getType() + ";");
				writer.println("");

			}

			if (ndecls != null)
				for (int i = 0; i < ndecls.size(); i++) {
					Binding b = ndecls.get(i);
					IType type = b.getExpr().getType();
					if (type == IntType.singleton || type == BoolType.singleton)
						writer.println(".field public loc_" + String.format("%02d", i) + " I");

					else
					
						writer.println(".field public loc_" + String.format("%02d", i) + " L"
								+  type.compileType() + ";");
					
				}
			else
				for (int i = 0; i < params.size(); i++) {
					Parameter b = params.get(i);
					IType type = b.getType();
					if (type == IntType.singleton || type == BoolType.singleton)
						writer.println(".field public loc_" + String.format("%02d", i) + " I");

					else
					
						writer.println(".field public loc_" + String.format("%02d", i) + " L"
								+  type.compileType() + ";");

				}

			// write footer
			writer.println(".method public <init>()V");
			writer.println("aload_0");
			writer.println("invokespecial java/lang/Object/<init>()V");
			writer.println("return");
			writer.println(".end method");

			// Close file
			writer.close();

		}

		public void updateParentReference(CodeBlock code) {

			if (parentFrame == null)
				return;
			code.emit_comment("Parent Frame");
			code.emit_dup();
			code.emit_aload();
			code.emit_putfield(getType() + "/SL L" + parentFrame.getType() + ";"); // putfield
																					// frame_2/SL
																					// Lframe_1;

		}

		public String getType() {
			return "frame_" + number;
		}
	}

	private String SP;

	ArrayList<String> code;
	ArrayList<StackFrame> frames;
	ArrayList<ClosureFrame> closure;
	StackFrame currentFrame;
	private int labelCount;

	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<StackFrame>();
		closure = new ArrayList<ClosureFrame>();
		labelCount = 0;
		SP = "0";
	}

	public CodeBlock(String sp) {
		code = new ArrayList<String>(100);
		frames = new ArrayList<StackFrame>();
		labelCount = 0;
		SP = sp;
		closure = new ArrayList<ClosureFrame>();
	}
	
	public void addClosure(ClosureFrame c){
		closure.add(c);
	}

	public String newLabel() {
		return LABEL + labelCount++;
	}

	public void emit_label(String label) {
		code.add(label + ":");

	}

	public void emit_push(int n) {
		code.add("sipush " + n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	public void emit_and() {
		code.add("iand");

	}

	public void emit_ifne(String label) {
		code.add("ifne " + label);
	}

	public void emit_if_icmpeq(String label) {
		code.add("if_icmpeq " + label);

	}

	public void emit_if_icmpne(String label) {
		code.add("if_icmpne " + label);

	}

	public void emit_iconst_(int n) {
		code.add("iconst_" + n);

	}

	public void emit_goto(String label) {
		code.add("goto " + label);
	}

	public void emit_if_icmple(String label) {
		code.add("if_icmple " + label);

	}

	public void emit_if_icmpge(String label) {
		code.add("if_icmpge " + label);

	}

	public void emit_new(String label) {
		code.add("new " + label);
	}

	public void emit_dup() {
		code.add("dup");
	}

	public void emit_invokespecial(String invoke) {
		code.add("invokespecial " + invoke);

	}

	public void emit_invokeinterface(String invoke) {
		code.add("invokeinterface " + invoke);

	}

	public void emit_putfield(String variable) {
		code.add("putfield " + variable);
	}

	public void emit_aload() {
		code.add("aload " + SP);
	}

	public void emit_aload(int n) {
		code.add("aload " + n);
	}


	public void emit_iload(int num) {
		code.add("iload " + num);
	}

	public void emit_checkcast() {
		code.add("checkcast " + currentFrame.getType());

	}

	public void emit_getfield(String field) {
		code.add("getfield " + field);
	}

	public void emit_getIdValue(Address address, IType type) {
		StackFrame current = currentFrame;
		for (int i = 0; i < address.getJumps(); i++) {
			code.add("getfield " + current.getType() + "/SL L" + current.parentFrame.getType() + ";");
			current = current.parentFrame;
		}
		
		
		if (type == IntType.singleton ||type == BoolType.singleton)
			emit_getfield(current.getType() + "/loc_" + String.format("%02d", address.getOffset()) + " I");
		else
			emit_getfield(current.getType() + "/loc_" + String.format("%02d", address.getOffset()) + " L"
					+ type.compileType() + ";");
	}

	public void emit_comment(String comment) {
		code.add("");
		code.add("; " + comment);
	}

	public void emit_pop() {
		code.add("pop");
	}

	public void emit_ifeq(String label) {
		code.add("ifeq " + label);
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");
		out.println("       return");
		out.println("");
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for (String s : code)
			out.println("       " + s);
	}

	private void dumpFrames() throws DuplicateIdentifierException, UndeclaredIdentifierException {
		for (StackFrame f : frames)
			f.dump();
		
		for (ClosureFrame f : closure)
			f.dump();
	}

	public void dump(String filename) throws FileNotFoundException, DuplicateIdentifierException, UndeclaredIdentifierException {
		dumpFrames();

		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
	}

	public StackFrame createFrame(List<Binding> ndecls, List<Parameter> params) {
		StackFrame frame = new StackFrame(ndecls, currentFrame, params);
		frames.add(frame);
		return frame;
	}

	public void pushFrame(StackFrame frame) {
		code.add("astore " + SP);
		currentFrame = frame;
	}

	public void pushFrame(StackFrame frame, int i) {
		code.add("astore " + i);
		currentFrame = frame;
	}

	public void popFrame() {
		this.emit_comment("Pop Frame");
		if (currentFrame.parentFrame == null) {
			code.add("aconst_null");
			code.add("astore " + SP);
		} else {
			code.add("aload " + SP);
			code.add("checkcast " + currentFrame.getType());
			code.add("getfield " + currentFrame.getType() + "/SL "
					+ this.reftypeToJasmin(currentFrame.parentFrame.getType()));
			code.add("astore " + SP);
		}

		currentFrame = currentFrame.parentFrame;
	}

	private String reftypeToJasmin(String type) {
		return "L" + type + ";";
	}

	public Ref createRef(IType type) {
		return new Ref(type);
	}

}