package AST;

import java.util.ArrayList;
import java.util.List;

import Main.CodeBlock;
import Main.CodeBlock.StackFrame;
import Type.ClosureType;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.Binding;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;
public class ASTDecl implements ASTNode {
	
	List<Binding> decl;

	ASTNode expr;
	IType type;
	
    public ASTDecl(List<Binding> decl, ASTNode expr)
    {
		this.decl = decl; 
		this.expr = expr;
    }
    
    public ASTDecl()
    {
    	decl = new ArrayList<Binding>();
    }
    
    public void addDecl(Binding a){
    	decl.add(a);
    }
    
    public void setExpre(ASTNode expr){
    	this.expr = expr;
    }


    public String toString() {
    		String s = "";
    		
    		for(Binding b : decl){
        		s += b.getId() + " = " + b.getExpr().toString() + " ";
    		}
    		
    		return "decl " + s + " in " + expr.toString() + " end";
    		

    		
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		IValue value;
		Environment<IValue> newEnv = env.beginScope();		

		
		for(Binding b : decl){

		IValue idValue = b.getExpr().eval(env);

		newEnv.assoc(b.getId(), idValue);
		}
		value = expr.eval(newEnv);
		newEnv.endScope();
	
		return value;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		code.emit_comment("Decl");
		CompilerEnvironment newEnv = env.beginScope();		

		// create frame
		StackFrame frame = code.createFrame(decl, null);

		//		decl.getExpr().compile(code);

		// store value
		// begin scope (push SP)
		code.emit_new(frame.getType());
		code.emit_dup();
		code.emit_invokespecial(frame.getType() + "/<init>()V");
		
		for(int i = 0; i < decl.size(); i++) {
			Binding b = decl.get(i);
			code.emit_dup();
			b.getExpr().compile(code,env);
			
			
			if(b.getExpr().getType() == IntType.singleton || b.getExpr().getType() == IntType.singleton)
				code.emit_putfield(frame.getType() + "/loc_"+ String.format("%02d", i)+" I");
			else
				code.emit_putfield(frame.getType() + "/loc_"+ String.format("%02d", i)+" L"+ b.getExpr().getType().compileType()+ ";");
			
		/*	if(b.getExpr().getType() instanceof RefType)
				code.emit_putfield(frame.getType() + "/loc_"+ String.format("%02d", i)+" L" +((RefType)b.getExpr().getType()).getName()+ ";");
			else if(b.getExpr().getType() instanceof ClosureType)
				code.emit_putfield(frame.getType() + "/loc_"+ String.format("%02d", i)+" L"+((ClosureType)b.getExpr().getType()).getInterface()+ ";");
			else
				code.emit_putfield(frame.getType() + "/loc_"+ String.format("%02d", i)+" I");
			
			
			*/
			
			
			
			newEnv.addId(b.getId());


		}
		if(!frame.getType().equals("frame_0")){
			frame.updateParentReference(code);
		}
		
		code.pushFrame(frame);
		// associate id to address
		expr.compile(code,newEnv);
		// end scope (pop SP)
		code.popFrame();
		newEnv.endScope();

	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		Environment<IType> envLocal = env.beginScope();		

		for(Binding b : decl){
			IType type = b.typecheck(env) ;
			if(type == null){
				return null;
			}
			envLocal.assoc(b.getId(), type);

		}
		
		IType expType = expr.typecheck(envLocal);
		

		type = expType;
		return expType; //nao e pressiso checkar null
	}
	
	@Override
	public IType getType() {
		return type;
	}
}

