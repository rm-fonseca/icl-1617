package AST;

import Main.CodeBlock;
import Type.IType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public interface ASTNode {

	IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error; 
	
	void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException;

	IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException;

	
	IType getType();
}

