package AST;

import java.util.List;

import AST.value.Closure;
import Main.CodeBlock;
import Type.BoolType;
import Type.ClosureType;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.Parameter;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTCall implements ASTNode {

	ASTNode left;
	List<ASTNode> right;
	IType type;
	ClosureType closure;
	public ASTCall(ASTNode l, List<ASTNode> params) {
		left = l;
		right = params;
		type = null;
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {

		Closure c = (Closure) left.eval(env);
		// IValue arg = right.eval(env);
		Environment<IValue> newenv = c.getEnv();
		newenv = newenv.beginScope();

		for (int i = 0; i < right.size(); i++) {
			IValue arg = right.get(i).eval(env);
			IValue argUnchanged = arg;
			IType argumentType = c.getParameter().get(i).getType();
			while (true) {

				if (argumentType instanceof IntType) {
					if (arg instanceof IntValue)
						break;
					else
						throw new Error("Argument " + c.getParameter().get(i).getId() + " not int type");
				} else if (argumentType instanceof BoolType) {
					if (arg instanceof BoolValue)
						break;
					else
						throw new Error("Argument " + c.getParameter().get(i).getId() + " not bool type");
				} else if (argumentType instanceof RefType) {
					if (arg instanceof RefValue){
						argumentType = ((RefType)argumentType).getType();
						arg = ((RefValue)arg).getValue();
					}
					else
						throw new Error("Argument " + c.getParameter().get(i).getId() + " not ref type");
				} else
					throw new Error("Type Error on Call");

			}
			newenv.assoc(c.getParameter().get(i).getId(), argUnchanged);
		}

		return c.getBody().eval(newenv);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		String callParams = "";
		String returnType = "";
		code.emit_comment("Call");


		left.compile(code, env);
		for(ASTNode node : right){
			node.compile(code, env);
			if (node.getType() ==  IntType.singleton || node.getType() ==  BoolType.singleton)
				callParams += "I";	
			else
				callParams += "L" + node.getType().compileType() + ";";
			}

		
		if (closure.getType() == IntType.singleton || closure.getType() == BoolType.singleton)
			returnType += "I";
		else
			returnType += "L"+closure.getType().compileType()+";";
		
	    code.emit_invokeinterface(closure.compileType() + "/call("+callParams+")"+returnType+" "+(right.size()+1));

	 /*   
	    params

		dup
	       invokespecial closure_01/<init>()V
	       checkcast type_00
	       sipush 5
	       invokeinterface type_00/call(I)I 2
	       */
		
	}

	@Override
	public String toString() {
		return left.toString() + " ( " + right.toString() + ")";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		IType leftType = left.typecheck(env);
		if(!(leftType instanceof ClosureType))
			return null;
		closure = (ClosureType)leftType;
		// IValue arg = right.eval(env);
		Environment<IType> newenv = env.beginScope();
		newenv = newenv.beginScope();

		for (int i = 0; i < right.size(); i++) {
			IType arg = right.get(i).typecheck(env);
			IType argumentType = closure.getParameter().get(i).getType();
			
			
			if (!argumentType.equals(arg)) 
				return null;
			
			newenv.assoc(closure.getParameter().get(i).getId(), arg);
		}
		type = closure.getType();
		return type;
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
