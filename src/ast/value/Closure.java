package 	AST.value;

import java.util.List;

import AST.ASTNode;
import Type.ClosureType;
import Type.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.Parameter;
import util.UndeclaredIdentifierException;
import values.IValue;

public class Closure implements IValue {

	private List<Parameter> params;
	private ASTNode body;
	private Environment<IValue> env;

	public Closure(List<Parameter> params, ASTNode body, Environment<IValue> env) {
		this.params = params;
		this.body = body;
		this.env = env;
	}

	public  List<Parameter> getParameter() {
		return params;
	}


	public ASTNode getBody() {
		return body;
	}

	public Environment<IValue> getEnv() {
		return env;
	}

	@Override
	public IType getType() {
		/*try {

		Environment<IType> env = new Environment<IType>();
		for(Parameter p : params)
				env.assoc(p.getId(), p.getType());
	
		
		return body.typecheck(env);
		} catch (DuplicateIdentifierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UndeclaredIdentifierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		
		return null;
	}


}
