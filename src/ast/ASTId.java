package AST;
import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import util.CompilerEnvironment;
import util.CompilerEnvironment.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTId implements ASTNode {

	String id;
	IType type;
	public ASTId(String id)
	{
		this.id = id;
	}

	public IValue eval(Environment<IValue> env) 
			throws UndeclaredIdentifierException, ExecutionErrorException, Error { 
		return env.find(id); 
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		Address address = env.lookup(id);
		
		
		code.emit_comment("Retriving id= " + id );
		code.emit_aload();
		code.emit_checkcast();
		code.emit_getIdValue(address , type);

		

		// needs an environment -> Add it to the compile method signature
		// env.find(id); -> returns a pair (jumps, offset)
		// crawls the static link for the number of jumps
		// get the value from the frame in the given offset
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		
		type = env.find(id);
		return type;
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
}

