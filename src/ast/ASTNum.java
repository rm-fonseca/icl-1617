package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;
public class ASTNum implements ASTNode {

	int val;
	
	public ASTNum(int n) {
		val = n;
	}

	public IValue eval(Environment<IValue> env) throws Error {
		return new IntValue(val);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException {
		code.emit_push(val);
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException {
		return IntType.singleton;
	}
	
	@Override
	public IType getType() {
		return IntType.singleton;
	}
}
