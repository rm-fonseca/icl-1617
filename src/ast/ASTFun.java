package AST;

import java.io.FileNotFoundException;
import java.util.List;

import AST.value.Closure;
import Main.CodeBlock;
import Main.CodeBlock.ClosureFrame;
import Main.CodeBlock.StackFrame;
import Type.BoolType;
import Type.ClosureType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.Parameter;
import values.*;

public class ASTFun implements ASTNode {

	List<Parameter> params;
	ASTNode body;
	IType type;
	int number;
	static int fnumber = 0;


	public ASTFun(List<Parameter> params, ASTNode body) {
		this.params = params;
		this.body = body;
		number = fnumber++;
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		return new Closure(params, body, env);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException ,UndeclaredIdentifierException{
		
		ClosureFrame c = new ClosureFrame(env, body,params,(ClosureType)type );
		
		//c.createClosureFile();
		code.emit_new(((ClosureType)type).getClassName());
	    code.emit_dup();
	    code.emit_invokespecial(((ClosureType)type).getClassName() + "/<init>()V");
	}

	@Override
	public String toString() {
		String parameters = "";

		for (Parameter p : params) {
			parameters += p.getId() + ":" + p.getType() + " ";
		}

		return "fun " + parameters + "=> " + body.toString() + " end";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		Environment<IType> envN = env.beginScope();
		
		for(Parameter p : params)
			envN.assoc(p.getId(), p.getType());
		
		IType bodyType = body.typecheck(envN);
		if(bodyType == null)
			return null;
		type = new ClosureType(params, bodyType,number);
		
		return type;
	}

	@Override
	public IType getType() {
		return type;
	}

}
