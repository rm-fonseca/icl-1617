package AST;

import Main.CodeBlock;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		
		IValue leftV = left.eval(env);
		if(!(leftV instanceof IntValue))
			throw new Error("Add left value not an int");
		
		IValue rightV = right.eval(env);
		
		if(!(rightV instanceof IntValue))
			throw new Error("Add right value not an int");

		return new IntValue(((IntValue)leftV).getValue() + ((IntValue) rightV).getValue());

	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env)
			throws DuplicateIdentifierException, UndeclaredIdentifierException {
		code.emit_comment("ADD");
		
		left.compile(code, env);
		right.compile(code, env);
		code.emit_add();
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		if (l != IntType.singleton)
			return null;
		IType r = right.typecheck(env);
		if (r != IntType.singleton)
			return null;

		return IntType.singleton;

	}

	@Override
	public IType getType() {
		return IntType.singleton;
	}
}
