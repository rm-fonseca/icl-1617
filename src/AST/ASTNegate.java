package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTNegate implements ASTNode {

	ASTNode val;

	public ASTNegate(ASTNode value) {
		val = value;
	}

	@Override
	public String toString() {
		return "!" + val.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env)
			throws DuplicateIdentifierException, UndeclaredIdentifierException {
		val.compile(code, env);
		String label1 = code.newLabel();
		String label2 = code.newLabel();
		code.emit_iconst_(0);
		code.emit_if_icmpeq(label1);
		code.emit_iconst_(0);
		code.emit_goto(label2);
		code.emit_label(label1);
		code.emit_iconst_(1);
		code.emit_label(label2);
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		IValue nodeV = val.eval(env);
		if(!(nodeV instanceof BoolValue))
			throw new Error("Negate value not an bool");
		
		
		return ((BoolValue) nodeV).getValue() ? new BoolValue(false) : new BoolValue(true);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		if (val.typecheck(env) == BoolType.singleton)
			return BoolType.singleton;

		return null;
	}
	
	@Override
	public IType getType() {
		return BoolType.singleton;
	}
}
