package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTDiff implements ASTNode {

	ASTNode left, right;

	public ASTDiff(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public String toString() {
		return left.toString() + " != " + right.toString();
	}
	
	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		left.compile(code,env);
		right.compile(code,env);
		String label1 = code.newLabel();
		String label2 = code.newLabel();
		code.emit_if_icmpeq(label1);
		code.emit_iconst_(1);
		code.emit_goto(label2);
		code.emit_label(label1);
		code.emit_iconst_(0);
		code.emit_label(label2);
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		IValue leftV = left.eval(env);
		IValue rightV = right.eval(env);

		if (!(leftV instanceof IntValue) || !(leftV instanceof BoolValue))
			throw new Error("Diff left value not a valid Value");

		if (!(rightV instanceof IntValue) || !(rightV instanceof BoolValue))
			throw new Error("Diff right value not a valid Value");

		if (!(leftV.getClass().equals( rightV.getClass())))
			throw new Error("Diff right and left value not same type");

		
		
		
		if (leftV instanceof IntValue)
			return ((IntValue) leftV).getValue() != ((IntValue) leftV).getValue()
					? new BoolValue(true) : new BoolValue(false);

		else
			return ((BoolValue) leftV).getValue() != ((BoolValue) leftV).getValue()
					? new BoolValue(true) : new BoolValue(false);

	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType l = left.typecheck(env);
		IType r = right.typecheck(env);

		if((l == BoolType.singleton && r == BoolType.singleton) || (l == IntType.singleton && r == IntType.singleton))
			return BoolType.singleton;
		
		
		
		return null;
	}
	
	@Override
	public IType getType() {
		return BoolType.singleton;
	}
}
