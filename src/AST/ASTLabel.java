package AST;

import Main.CodeBlock;
import Type.FieldType;
import Type.IType;
import Type.ObjectType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTLabel implements ASTNode {

	ASTNode node;
	String label;
	IType type;
	public ASTLabel(ASTNode node, String label) {
		this.node = node;
		this.label = label;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		IValue ref = node.eval(env);
		
		if(ref instanceof FieldsValue)
			return ((FieldsValue) ref).getValue(label).getExpr().eval(env);
		
		if(ref instanceof ObjectValue)
			return ((ObjectValue) ref).evalLabel(label, env);

		
		
			throw new Error("Label not a valid record");

		
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
	}

	@Override
	public String toString() {
		return node.toString() + "." + label;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType ref = node.typecheck(env);
		
		if((ref instanceof FieldType)){
			type = ref;
			return  ((FieldType) ref).typecheckId(label, env);
		}
		
		if((ref instanceof ObjectType)){
			type = ref;
			return  ((ObjectType) ref).typecheckId(label, env);
		}
		
		return null;
	}
	
	@Override
	public IType getType() {
		return type;
	}
}
