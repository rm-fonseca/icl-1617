package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTIf implements ASTNode {

	ASTNode condition;
	ASTNode exp1;
	ASTNode exp2;
	IType type;

	public ASTIf(ASTNode condition,ASTNode exp1,ASTNode exp2) {
		this.condition = condition;
		this.exp1 = exp1;
		this.exp2 = exp2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		
		IValue conditionV = condition.eval(env);
		
		if(!(conditionV instanceof BoolValue))
			throw new Error("If condition not a bool value");


		
		
		if(((BoolValue)condition.eval(env)).getValue())
			return  exp1.eval(env);
		else
			return  exp2.eval(env);


	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		/*left.compile(code,env);
		right.compile(code,env);
		*/
		
		String labelElse = code.newLabel();
		String labelEndIf = code.newLabel();
		condition.compile(code,env);
		code.emit_ifeq(labelElse);;
		exp1.compile(code,env);
		code.emit_goto(labelEndIf);
		code.emit_label(labelElse);
		exp2.compile(code,env);
		code.emit_label(labelEndIf);

	}

	@Override
	public String toString() {
		return "if " + condition.toString() + " then " + exp1.toString() + " else " + exp2.toString() + " end";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType conditionType = this.condition.typecheck(env);
		if(conditionType != BoolType.singleton)
			return null;
		IType exp1Type = this.exp1.typecheck(env);
		IType exp2Type = this.exp2.typecheck(env);
		
		
		if((exp1Type == null) || (exp1Type != exp2Type))
			return null;
		
		
		type = exp1Type;
		return exp1Type;
	}
	
	@Override
	public IType getType() {
		return type;
	}
}
