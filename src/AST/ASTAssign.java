package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTAssign implements ASTNode {

	ASTNode id;
	ASTNode exp;
	IType type;

	public ASTAssign(ASTNode id,ASTNode exp) {
		this.id = id;
		this.exp = exp;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		IValue value = exp.eval(env);
		IValue idV = id.eval(env);
		if(!(idV instanceof RefValue))
			throw new Error("Assign left side not a ID");
		((RefValue) idV).setValue(value);
		
		return value;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		code.emit_comment("Assign");

		id.compile(code, env);
		code.emit_dup();
		exp.compile(code, env);
		if(exp.getType() instanceof RefType)
			code.emit_putfield(((RefType)id.getType()).getName() + "/value L" + ((RefType)exp).getName() + ";");		 
		else
			code.emit_putfield(((RefType)id.getType()).getName()+ "/value I");	
		
		if(exp.getType() instanceof RefType)
			code.emit_getfield (((RefType)id.getType()).getName() + "/value L" + ((RefType)exp).getName() + ";");		 
		else
			code.emit_getfield (((RefType)id.getType()).getName() + "/value I");	
			
		
	}

	@Override
	public String toString() {
		return id.toString() + " := " + exp.toString();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IType id = this.id.typecheck(env);
		IType exp = this.exp.typecheck(env);
		
		if(id != null && exp != null &&  id.equals(new RefType(exp))){
			type = exp;
			return exp;
		
		}
		return null;
	}

	@Override
	public IType getType() {
		return type;
	}
}
