package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTLess implements ASTNode {

	ASTNode left, right;

	public ASTLess(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}


	@Override
	public String toString() {
		return left.toString() + " < " + right.toString();
	}
	
	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		left.compile(code,env);
		right.compile(code,env);
		String label1 = code.newLabel();
		String label2 = code.newLabel();
		code.emit_if_icmpge(label1);
		code.emit_iconst_(1);
		code.emit_goto(label2);
		code.emit_label(label1);
		code.emit_iconst_(0);
		code.emit_label(label2);
	}
	

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		IValue leftV = left.eval(env);
		if(!(leftV instanceof IntValue))
			throw new Error("Less left value not an int");
		
		IValue rightV = right.eval(env);
		
		if(!(rightV instanceof IntValue))
			throw new Error("Less right value not an int");

		return ((IntValue)leftV).getValue() <
		((IntValue)rightV).getValue()  ?new BoolValue(true) : new BoolValue(false);	}


	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		if (l != IntType.singleton)
			return null;
		IType r = right.typecheck(env);
		if (r != IntType.singleton)
			return null;

		return BoolType.singleton;
	}
	
	@Override
	public IType getType() {
		return BoolType.singleton;
	}
}
