package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;
public class ASTBool implements ASTNode {

	Boolean val;
	
	public ASTBool(Boolean n) {
		val = n;
	}

	public IValue eval(Environment<IValue> env) throws Error {
		return new BoolValue(val);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException {
		code.emit_push((val) ? 1 : 0);
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException {
		return BoolType.singleton;
	}
	

	@Override
	public IType getType() {
		return IntType.singleton;
	}
}
