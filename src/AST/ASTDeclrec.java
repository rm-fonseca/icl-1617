package AST;

import java.util.ArrayList;
import java.util.List;

import AST.value.Closure;
import Main.CodeBlock;
import Main.CodeBlock.StackFrame;
import Type.ClosureType;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.Binding;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeBinding;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTDeclrec implements ASTNode {

	List<TypeBinding> decl;

	ASTNode expr;
	IType type;

	public ASTDeclrec(List<TypeBinding> decl, ASTNode expr) {
		this.decl = decl;
		this.expr = expr;
	}

	public void setExpre(ASTNode expr) {
		this.expr = expr;
	}

	public String toString() {
		String s = "";

		for (TypeBinding b : decl) {
			s += b.getId() + ":" + b.getType() + " = " + b.getExpr().toString() + " ";
		}

		return "decl " + s + " in " + expr.toString() + " end";

	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		IValue value;
		Environment<IValue> newEnv = env.beginScope();

		for (TypeBinding b : decl) {

			IValue idValue = b.getExpr().eval(newEnv);

			if (idValue instanceof Closure) {
				//BATOTA
			}else
			if (!idValue.getType().equals(b.getType()))
				throw new Error("DeclRec diferent types");

			

			newEnv.assoc(b.getId(), idValue);
		}
		value = expr.eval(newEnv);
		newEnv.endScope();

		return value;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env)
			throws DuplicateIdentifierException, UndeclaredIdentifierException {
		CompilerEnvironment newEnv = env.beginScope();

		List<Binding> bindings = new ArrayList<Binding>();

		for (TypeBinding b : decl) {

			bindings.add(new Binding(b.getId(), b.getExpr()));
		}

		// create frame
		StackFrame frame = code.createFrame(bindings, null);

		// decl.getExpr().compile(code);

		// store value
		// begin scope (push SP)
		code.emit_new(frame.getType());
		code.emit_dup();
		code.emit_invokespecial(frame.getType() + "/<init>()V");
		code.pushFrame(frame);
		for (int i = 0; i < decl.size(); i++) {
			code.emit_aload();
			TypeBinding b = decl.get(i);
			newEnv.addId(b.getId());
			b.getExpr().compile(code, newEnv);

			if (b.getExpr().getType() == IntType.singleton || b.getExpr().getType() == IntType.singleton)
				code.emit_putfield(frame.getType() + "/loc_" + String.format("%02d", i) + " I");
			else
				code.emit_putfield(frame.getType() + "/loc_" + String.format("%02d", i) + " L"
						+ b.getExpr().getType().compileType() + ";");



		}
		if (!frame.getType().equals("frame_0")) {
			frame.updateParentReference(code);
		}

		// associate id to address
		expr.compile(code, newEnv);
		// end scope (pop SP)
		code.popFrame();
		newEnv.endScope();

	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		Environment<IType> envLocal = env.beginScope();

		for (TypeBinding b : decl) {
			envLocal.assoc(b.getId(), b.getType());
			IType type = b.typecheck(envLocal);
			if (type == null || !b.getType().equals(type)) {
				return null;
			}

	/*		if (type instanceof Closure) {
				((ClosureType) type).getEnv().assoc(b.getId(), type);
			}
*/

		}

		IType expType = expr.typecheck(envLocal);

		type = expType;
		return expType; // nao e pressiso checkar null
	}

	@Override
	public IType getType() {
		return type;
	}
}
