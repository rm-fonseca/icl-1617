package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTWhile implements ASTNode {

	ASTNode condition;
	ASTNode exp;

	public ASTWhile(ASTNode condition,ASTNode exp) {
		this.condition = condition;
		this.exp = exp;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		try{
		while(((BoolValue)condition.eval(env)).getValue())
			exp.eval(env);
		}catch(ClassCastException e){
			throw new Error("While condition not a boolean");
		}
		return new BoolValue(false);

	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		code.emit_comment("While");

		String labelBeginWhile = code.newLabel();
		String labelEndWhile = code.newLabel();
		code.emit_label(labelBeginWhile);
		condition.compile(code,env);
		code.emit_ifeq(labelEndWhile);;
		exp.compile(code,env);
		code.emit_pop();
		code.emit_goto(labelBeginWhile);
		code.emit_label(labelEndWhile);
		code.emit_iconst_(0); // false no final do while
	}


	@Override
	public String toString() {
		return "while " + condition.toString() + " do " + exp.toString() + " end";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IType conditionType = this.condition.typecheck(env);
		if(conditionType != BoolType.singleton)
			return null;
		IType expType = this.exp.typecheck(env);
		
		
		if((expType == null))
			return null;
		
		
		
		return BoolType.singleton;
	}
	
	@Override
	public IType getType() {
		return BoolType.singleton;
	}
	
}
