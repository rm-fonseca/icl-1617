package AST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import AST.value.Closure;
import Main.CodeBlock;
import Type.BoolType;
import Type.FieldType;
import Type.IType;
import Type.IntType;
import util.Binding;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.Field;
import util.UndeclaredIdentifierException;
import util.Parameter;
import values.*;

public class ASTFields implements ASTNode {

	List<Binding> bindings;
	IType type;

	public ASTFields(List<Binding> bindings) {
		this.bindings = bindings;
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		HashMap<String,Binding> fields = new HashMap<String,Binding>();
		
		for(Binding b : bindings){
			fields.put(b.getId(), b);
		}
		return new FieldsValue(fields);
	}
	
	/*
		HashMap<String,IValue> fields = new HashMap<String,IValue>();
		
		for(Binding b : bindings){
			fields.put(b.getId(), b.getExpr().eval(env));
		}
		return new FieldsValue(fields);
	 */

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException {

		
	}

	@Override
	public String toString() {
		String parameters = "";

		for (Binding p : bindings) {
			parameters += p.getId() + "=" + p.getExpr().toString() + ",";
		}
		if(parameters.length() > 0)
			parameters = parameters.substring(0, parameters.length()-1);

		return "{" + parameters + "}";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		HashMap<String,Binding> fields = new HashMap<String,Binding>();
		
		for(Binding b : bindings){
			fields.put(b.getId(), b);
		}
		type = new FieldType(fields);
		return type;
	}

	@Override
	public IType getType() {
		return type;
	}

}
