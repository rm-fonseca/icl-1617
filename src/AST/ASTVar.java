package AST;

import Main.CodeBlock;
import Main.CodeBlock.Ref;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTVar implements ASTNode {

	ASTNode node;
	private IType type;

	public ASTVar(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		return new RefValue(node.eval(env));
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env)
			throws DuplicateIdentifierException, UndeclaredIdentifierException {
		/*
		 * left.compile(code,env); right.compile(code,env); code.emit_add();
		 */
		code.emit_comment("Create Ref");
		Ref ref = code.createRef(type);
		ref.createRefFile();
		code.emit_new(ref.getType());
		code.emit_dup();
		code.emit_invokespecial(ref.getType() + "/<init>()V");
		code.emit_dup();
		node.compile(code, env);
/*
		if (((RefType) type).getType() instanceof RefType)
			code.emit_putfield(ref.getType() + "/value L" + ((RefType) ((RefType) type).getType()).getName() + ";");
		else
			code.emit_putfield(ref.getType() + "/value I");
*/
		if (((RefType) type).getType() == IntType.singleton || ((RefType) type).getType() == IntType.singleton)
			code.emit_putfield(ref.getType() + "/value I");
		else
			code.emit_putfield(ref.getType() + "/value L" + ((RefType) type).getType().compileType() + ";");


	}

	@Override
	public String toString() {
		return "var(" + node.toString() + ")";
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType nodeType = node.typecheck(env);

		if (nodeType == null)
			return null;

		this.type = new RefType(nodeType);
		return type;
	}

	@Override
	public IType getType() {
		return type;
	}

}
