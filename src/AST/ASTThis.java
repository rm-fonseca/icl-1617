package AST;

import Main.CodeBlock;
import Type.IType;
import Type.ObjectType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTThis implements ASTNode {

	String label;
	IType type;
	public ASTThis(String label) {
		this.label = label;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		IValue objectThis =env.find("THIS");
		
		
		
		if(!(objectThis instanceof ObjectValue))
			throw new Error("This not valid");

		
		return ((ObjectValue) objectThis).getValue(label).getExpr().eval(env);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{

	}

	@Override
	public String toString() {
		return "this." + label.toString();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType objectThis = env.find("THIS");
		
		
		
		if(!(objectThis instanceof ObjectType))
			throw new Error("This not valid");
		type = ((ObjectType) objectThis).typecheckId(label, env);
		
		return type;
	}
	
	@Override
	public IType getType() {
		return type;
	}
}
