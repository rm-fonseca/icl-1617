package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTSequence implements ASTNode {

	ASTNode left;
	ASTNode right;
	private IType type;

	public ASTSequence(ASTNode left,ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		left.eval(env);
		return right.eval(env);

	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		left.compile(code,env);
		code.emit_pop();
		right.compile(code,env);

	}

	@Override
	public String toString() {
		return left.toString() + " ; " + right.toString();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {


		IType l = left.typecheck(env);
		if( l == null)
			return null;
		
		type = right.typecheck(env);
		return type;
	}
	
	@Override
	public IType getType() {
		return type;
	}
}
