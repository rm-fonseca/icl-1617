package AST;

import Main.CodeBlock;
import Type.BoolType;
import Type.IType;
import Type.IntType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;


public class ASTAnd implements ASTNode {

	ASTNode left, right;

	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " && " + right.toString();
	}
	
	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		code.emit_comment("AND");

		left.compile(code,env);
		right.compile(code,env);
		code.emit_and();
	}

	@Override
	public IValue eval(Environment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		
		IValue leftV = left.eval(env);
		if(!(leftV instanceof BoolValue))
			throw new Error("Add left value not an bool");
		
		IValue rightV = right.eval(env);
		
		if(!(rightV instanceof BoolValue))
			throw new Error("Add right value not an bool");
		
		
		return ((BoolValue)leftV).getValue() && ((BoolValue)rightV).getValue() ? new BoolValue(true) : new BoolValue(false);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		if (l != BoolType.singleton)
			return null;
		IType r = right.typecheck(env);
		if (r != BoolType.singleton)
			return null;

		return BoolType.singleton;
	}


	@Override
	public IType getType() {
		return IntType.singleton;
	}
	
}
