package AST;

import Main.CodeBlock;
import Type.IType;
import Type.IntType;
import Type.RefType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTDeref implements ASTNode {

	ASTNode node;
	IType type;
	public ASTDeref(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, Error {
		
		IValue ref = node.eval(env);
		
		if(!(ref instanceof RefValue))
			throw new Error("Deref not a Ref");

		
		return ((RefValue) ref).getValue();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnvironment env) throws DuplicateIdentifierException, UndeclaredIdentifierException{
		/*left.compile(code,env);
		right.compile(code,env);
		code.emit_add();*/
		code.emit_comment("Deref");

		node.compile(code, env);

		
		if(((RefType)type).getType() == IntType.singleton || ((RefType)type).getType() == IntType.singleton)
			code.emit_getfield (((RefType)type).getName() + "/value I");	
		else
			code.emit_getfield (((RefType)type).getName() + "/value L" + ((RefType)type).getType().compileType() + ";");		 
	
	}

	@Override
	public String toString() {
		return "*" + node.toString();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {

		IType ref = node.typecheck(env);
		
		if((ref instanceof RefType)){
			type = ref;
			return ((RefType) ref).getType();
		}
		
		return null;
	}
	
	@Override
	public IType getType() {
		return type;
	}
}
