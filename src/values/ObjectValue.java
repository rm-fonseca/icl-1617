package values;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import Type.IType;
import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.Field;
import util.UndeclaredIdentifierException;


public class ObjectValue implements IValue {
	private HashMap<String,Binding> value;
	private static String THIS = "THIS";
	public ObjectValue(HashMap<String,Binding> value) {
		this.value = value;
	}

	public  Binding getValue(String id) {
		return value.get(id);
	}
	
	@Override
	public String toString() {
		
		String fields = "";
		
		for(Entry<String, Binding> b : value.entrySet()){
			
			fields += b.getKey() + "=" + b.getValue().getExpr().toString()+ ",";
			
		}
		if(fields.length() > 0)
			fields = fields.substring(0, fields.length()-1);
		
		return "{{" + fields + "}}";
	}
	
	public IValue evalLabel(String id,Environment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException, ExecutionErrorException, Error{
		Environment<IValue> nEnv = env.beginScope();
		nEnv.assoc(THIS, this);
		IValue value = getValue(id).getExpr().eval(nEnv);
		nEnv.endScope();
		return value;
		
	}

	@Override
	public IType getType() {
		return null;
	}
	
}
