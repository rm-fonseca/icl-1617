package values;

import Type.BoolType;
import Type.IType;

public class BoolValue implements IValue {
	private Boolean value;
	
	public BoolValue(Boolean value) {
		this.value = value;
	}

	public Boolean getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Boolean.toString(value);
	}

	@Override
	public IType getType() {
		return BoolType.singleton;
	}
	
	
}
