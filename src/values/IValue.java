package values;

import Type.IType;

public interface IValue {

	
	public IType getType();
	
}
