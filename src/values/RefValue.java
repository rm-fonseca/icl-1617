package values;

import Type.BoolType;
import Type.IType;
import Type.RefType;

public class RefValue implements IValue {
	private IValue value;
	
	public RefValue(IValue value) {
		this.value = value;
	}

	public IValue getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "REF";
	}
	
	public void setValue(IValue value){
		this.value = value;
	}
	
	@Override
	public IType getType() {
		return new RefType(value.getType());
	}
	
}
