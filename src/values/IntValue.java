package values;

import Type.BoolType;
import Type.IType;
import Type.IntType;

public class IntValue implements IValue {
	private int value;
	
	public IntValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
	
	@Override
	public IType getType() {
		return IntType.singleton;
	}
}
