package values;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import Type.IType;
import util.Binding;
import util.Field;


public class FieldsValue implements IValue {
	private HashMap<String,Binding> value;
	
	public FieldsValue(HashMap<String,Binding> value) {
		this.value = value;
	}

	public  Binding getValue(String id) {
		return value.get(id);
	}

	@Override
	public String toString() {
		
		String fields = "";
		
		for(Entry<String, Binding> b : value.entrySet()){
			
			fields += b.getKey() + "=" + b.getValue().getExpr().toString()+ ",";
			
		}
		if(fields.length() > 0)
			fields = fields.substring(0, fields.length()-1);
		
		return "{" + fields + "}";
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
