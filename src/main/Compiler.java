package Main;

import Parser.ParseException;
import Parser.Parser;
import Type.IType;
import util.CompilerEnvironment;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import AST.ASTNode;

public class Compiler {

  public static void main(String args[]) throws ParseException, FileNotFoundException, DuplicateIdentifierException, UndeclaredIdentifierException {
    Parser parser = new Parser(System.in);
    ASTNode exp;
	CompilerEnvironment env = new CompilerEnvironment();	

 //   try {
      exp = parser.Start();

      CodeBlock code = new CodeBlock();
		Environment<IType> envItype = new Environment<>();
		if(exp.typecheck(envItype) == null)
			System.out.println("TypeCheck Error!");
		else
      try {
		exp.compile(code,env);
	} catch (DuplicateIdentifierException e) {
		System.out.println("Duplicated identifier " + e.getId() +"!");
		e.printStackTrace();
	} catch (UndeclaredIdentifierException e) {
		System.out.println("Undeclared identifier " + e.getId() +"!");
		e.printStackTrace();
	}

      code.dump("Demo.j");

   /* } catch (Exception e) {
      System.out.println ("Syntax Error!");
    }*/
  }
  
	public static void compile(String s) throws ParseException, FileNotFoundException, DuplicateIdentifierException, UndeclaredIdentifierException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode n = parser.Start();
		CompilerEnvironment env = new CompilerEnvironment();	
        CodeBlock code = new CodeBlock();
		n.compile(code,env);
		
		code.dump("Demo.j");
	}

}
