package Type;

import java.util.HashMap;

import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;

public class FieldType implements IType {
	private HashMap<String,Binding> value;

	   public FieldType(HashMap<String,Binding> value) {
			this.value = value;
	   }
		@Override
		public String toString(){
			return "bool";
		}
		
		
		public IType typecheckId(String id,Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException  {
			IType type = value.get(id).getExpr().typecheck(env);
			return type;
			
		}
		@Override
		public String compileType() {
			return null;
		}
}
