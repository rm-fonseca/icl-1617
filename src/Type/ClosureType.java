package Type;

import java.util.List;

import AST.ASTNode;
import util.Environment;
import util.Parameter;
import values.IValue;

public class ClosureType implements IType {

	private ClosureType(int type) {
		this.number = type;
	}
	IType output;
	int number;
	private List<Parameter> params;
	
	public ClosureType(List<Parameter> params, IType output, int number) {
		this.params = params;
		this.number = number;
		this.output = output;
	}
	
	public ClosureType(List<Parameter> params, IType output) {
		this.params = params;
		this.number = -1;
		this.output = output;
	}

	public List<Parameter> getParameter() {
		return params;
	}

	@Override
	public String toString() {
		return "type_" + String.format("%02d", number);
	}

	public String getClassName() {
		return "closure_" + String.format("%02d", number);
	}

	@Override
	public String compileType() {
		return toString();
	}

	public IType getType() {
		return output;
	}
	
	public int getNumber(){
		return number;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof ClosureType) ||((ClosureType)obj).getParameter().size() != params.size() || !(output.equals(((ClosureType)obj).getType())))
			return false;
		ClosureType c = ((ClosureType)obj);
		
		for(int i = 0 ; i < params.size() ; i++ ){
			
			if(!(c.getParameter().get(i).getType().equals(params.get(i).getType())))
				return false;
		}
		
		//batota
		number = c.getNumber();
		
		return true;
				
	}
	
}
