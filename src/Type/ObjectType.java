package Type;

import java.util.HashMap;

import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ObjectType implements IType {
	private HashMap<String,Binding> value;
	private static String THIS = "THIS";

	   public ObjectType(HashMap<String,Binding> value) {
			this.value = value;
	   }
	   
		@Override
		public String toString(){
			return "object";
		}
		
		
		public IType typecheckId(String id,Environment<IType> env) throws DuplicateIdentifierException, UndeclaredIdentifierException  {
			Environment<IType> nEnv = env.beginScope();
			nEnv.assoc(THIS, this);
			IType type = value.get(id).getExpr().typecheck(nEnv);
			nEnv.endScope();
			return type;
			
		}

		@Override
		public String compileType() {
			return null;
		}
}
