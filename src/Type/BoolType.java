package Type;

public class BoolType implements IType {
	   private BoolType() {
	   }
	   public static IType singleton = new BoolType();
	   
		@Override
		public String toString(){
			return "bool";
		}

		@Override
		public String compileType() {
			return "I";
		}
}
