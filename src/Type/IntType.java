package Type;

public class IntType implements IType {
	   private IntType() {
	   }
	   public static IType singleton = new IntType();
	   
		@Override
		public String toString(){
			return "int";
		}
		
		@Override
		public String compileType() {
			return "I";
		}
		
}
