package Type;

public class RefType implements IType {

	private IType type;

	public RefType(IType type) {
		this.type = type;
	}

	public IType getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof RefType))
			return false;
		
		
		return ((RefType)obj).getType().equals(type);
				
	}
	
	@Override
	public String toString(){
		return "ref(" + type.toString() + ")";
	}
	
	public String getName(){
		
		String type = "ref_";
		IType ref = this;
		while (ref  instanceof RefType) {
			ref = ((RefType) ref).getType();

			if (ref instanceof RefType) {
				type+= "ref_";
			} else if (ref == IntType.singleton) {
				type+= "int";

			} else if (ref == BoolType.singleton) {
				type+= "bool";

			}

		}
		
		return type;
		
	}
	
	@Override
	public String compileType() {
		return getName();
	}

}
